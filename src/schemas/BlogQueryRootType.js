import PostType from "./postType";
import Authors from "../data/authors";
import AsyncType from "./asyncType";
import CommentType from "./commentType";
import Posts from "../data/posts";
import AuthorType from "./authorType";
import fetch from "node-fetch";
import { GraphQLList, GraphQLObjectType, GraphQLNonNull, GraphQLID} from 'graphql/type';
const BlogQueryRootType = new GraphQLObjectType({
    name: 'Query',
    description: 'Blog Application Schema Query Root',
    fields: () => ({
        authors: {
            type: new GraphQLList(AuthorType),
            description: 'List of all the Authors',
            resolve: function () {
                return Authors;
            }
        },
        posts: {
            type: new GraphQLList(PostType),
            description: "List of All the Posts",
            resolve: function () {
                return Posts;
            }
        },
        asyncExampleList: {
            type: new GraphQLList(AsyncType),
            description: 'A simple Async Example',
            async resolve () {
                const posts = await fetch('https://jsonplaceholder.typicode.com/posts');
                return posts.json();
            }
        },
        asyncExample: {
            type: AsyncType,
            description: 'To fetch a single Post from the List',
            args: {id: {type: new GraphQLNonNull(GraphQLID)}},
            async resolve (parentObj, args) {
                console.log(args.id   );
                //this is something we pass from the client side
                const post = await fetch(`https://jsonplaceholder.typicode.com/posts/${args.id}`);
                return post.json();
            }
        },

        comments: {
            type: new GraphQLList(CommentType),
            description: 'TO Fetch a list of Comments for a Specific ID',
            args: { id: {type: new GraphQLNonNull(GraphQLID)}},
            async resolve (parentObj ,args) {
                const post = await fetch(`https://jsonplaceholder.typicode.com/posts/${args.id}/comments`);
                return post.json();
            }

        }
    })
});

export default BlogQueryRootType;