import fetch from "node-fetch";
import { GraphQLObjectType, GraphQLInt, GraphQLNonNull, GraphQLString, GraphQLList} from 'graphql/type';
import CommentType from './commentType';
const AsyncType = new GraphQLObjectType({
    name: 'AsyncExample',
    description: 'This is a simple Example',
    /* We can write an object or else we can write a function which returns an object*/
    fields: {
        userId: {type: new GraphQLNonNull(GraphQLInt)},
        id: {type: GraphQLInt},
        title: {type: new GraphQLNonNull(GraphQLString)},
        body: {type: new GraphQLNonNull(GraphQLString)},
        comments: {
            type: new GraphQLList(CommentType),
            async resolve (parentObj) {
                const post = await fetch (`https://jsonplaceholder.typicode.com/posts/${parentObj.id}/comments`);
                return post.json();
            }
        }
    }
});


export default AsyncType;