import { GraphQLObjectType } from 'graphql/type';
import createObj from './asyncMutation';
const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: () => ({createObj})
});

export default Mutation;