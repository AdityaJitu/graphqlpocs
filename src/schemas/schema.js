import { GraphQLSchema } from 'graphql/type';
import BlogQueryRootType from './BlogQueryRootType';
import Mutation from "./mutation";


const BlogAppSchema = new GraphQLSchema({
    query: BlogQueryRootType, //only for getting data
    mutation: Mutation //update, delete, put data 
});

export default BlogAppSchema;