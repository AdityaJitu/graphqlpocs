import AsyncType from "./asyncType";
import { GraphQLObjectType, GraphQLInt, GraphQLString} from 'graphql/type';
const createObj =  {
    type: AsyncType,
    args: {
        userId: {type: GraphQLInt},
        title: {type: GraphQLString},
        body: {type: GraphQLString}
    },
    resolve(parentObj, args) {
        let stuff = {};
        stuff.userId = args.userId;
        stuff.title = args.title;
        stuff.body = args.body;
        console.log(stuff);
        return stuff;
    }
}
export default createObj;

/**
 * 
 * mutation ($userId: Int , $body:String, $title:String) {
  createObj(title: $title, userId:$userId, body: $body) {
    userId, title, body
  }
}


{
    "userId": 1,
    "title": "Test Titile",
    "body": "Sample Body"
  }
 */
