import Authors from "../data/authors";
import find from "lodash/find";
import AuthorType from './authorType';
import { GraphQLObjectType, GraphQLString, GraphQLNonNull} from 'graphql/type';
const PostType = new GraphQLObjectType({
    name: 'Post',
    description: 'This represent a Post',
    fields: () => ({
        id: {type: new GraphQLNonNull(GraphQLString)},
        title: { type: new GraphQLNonNull(GraphQLString)},
        body: {type: GraphQLString},
        author: {
            type: AuthorType,
            resolve: function (post) {
                return find(Authors, a => a.id === post.author_id);
            }
        }
    })
});

export default PostType;