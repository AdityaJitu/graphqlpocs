import {GraphQLObjectType, GraphQLInt, GraphQLString, GraphQLNonNull} from 'graphql/type';
const CommentType = new GraphQLObjectType({
    name: 'Comments',
    description: 'A short description on Comments',
    fields: () => ({
        postId: { type: GraphQLInt},
        id: { type: GraphQLInt},
        name: { type: new GraphQLNonNull(GraphQLString)},
        email: {type: new GraphQLNonNull(GraphQLString)},
        body: {type: GraphQLString}
    })
});

export default  CommentType;