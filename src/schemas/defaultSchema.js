import { GraphQLObjectType,
    GraphQLNonNull,
    GraphQLString,
    GraphQLSchema } from 'graphql/type';

var type1 = new GraphQLObjectType({
    name: 'Example',
    description: 'this is a POC',
    fields: () => ({
       hello:  {
           type: new GraphQLNonNull(GraphQLString), 
           resolve: () => "Hi There"
        }
    })
});

const typeAppSchema = new GraphQLSchema({
    query: type1
})

/**
 * To fetch data from the api just using the url
 * http://localhost:3000/?query={hello}
 *
 */
export default typeAppSchema;