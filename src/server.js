import express from 'express';
import {urlencoded, json} from 'body-parser';
import graphqlHTTP from 'express-graphql';
import schema from './schemas/schema';
import defaultSchema from './schemas/defaultSchema';
let port = 5000;

const app = express();
// http://localhost:5000/world?query={%20authors%20{%20name%20}%20}
app.use(urlencoded({extended: false}));
app.use(json());
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    if (req.method === 'OPTIONS') {
        res.sendStatus(200);
    } else {
        next();
    }
});


app.use('/graphql', graphqlHTTP({
    schema: schema,
    graphiql: false
}));
app.use('/devMode', graphqlHTTP({
    schema: schema,
    graphiql: true
}));
app.use('/', graphqlHTTP({
    schema: defaultSchema,
    graphiql: true
}));
app.listen(port);
console.log('the server is running at' + port);